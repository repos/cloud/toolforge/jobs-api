---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: jobs-api
  labels:
    {{- include "jobs-api.labels" . | nindent 4 }}
