---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: jobs-api
  labels:
    name: jobs-api
    {{- include "jobs-api.labels" . | nindent 4 }}
  annotations:
    # use https://github.com/stakater/reloader (via the cert-manager deployment)
    # to restart when certificates get renewed
    configmap.reloader.stakater.com/reload: "nginx-config,harbor-config"
    secret.reloader.stakater.com/reload: "{{ .Release.Name }}-api-gateway-server"
spec:
  replicas: {{ .Values.replicas }}
  selector:
    matchLabels:
      name: jobs-api
      {{- include "jobs-api.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        name: jobs-api
        {{- include "jobs-api.selectorLabels" . | nindent 8 }}
    spec:
      nodeSelector:
        kubernetes.wmcloud.org/nfs-mounted: "true"
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels:
              name: jobs-api
              {{- include "jobs-api.selectorLabels" . | nindent 14 }}
      containers:
      - name: webservice
        image: {{ .Values.webservice.image.name }}:{{ .Values.webservice.image.tag }}
        imagePullPolicy: {{ .Values.webservice.image.pullpolicy }}
        # Needed to be able to read the NFS homes for certificates
        securityContext:
          runAsUser: 0
          allowPrivilegeEscalation: false
        resources: {}
        env:
          - name: "DEBUG"
            value: "{{ .Values.webservice.debug }}"
          - name: "PORT"
            value: "{{ .Values.webservice.port }}"
          - name: "ADDRESS"
            value: "{{ .Values.webservice.address }}"
          - name: "SKIP_METRICS"
            value: "{{ .Values.webservice.skip_metrics }}"
          - name: "SKIP_IMAGES"
            value: "{{ .Values.webservice.skip_images }}"
        volumeMounts:
        - mountPath: /data/project
          name: home
        - mountPath: /etc/wmcs-project
          name: wmcs-project
          readOnly: true
        - mountPath: /var/lib/sss/pipes
          name: sssd-pipes
        - mountPath: /etc/jobs-api/harbor.json
          subPath: harbor.json
          name: harbor-config
          readOnly: true
        - name: nsswitch
          mountPath: /etc/nsswitch.conf
          readOnly: true
      - name: nginx
        image: {{ .Values.nginx.image.name }}:{{ .Values.nginx.image.nginxTag }}
        imagePullPolicy: Always
        ports:
        - containerPort: 8443
          name: https
          protocol: TCP
        - containerPort: 9000
          name: metrics
          protocol: TCP
        resources: {}
        volumeMounts:
        - mountPath: /etc/nginx/api-gateway-ssl
          name: api-gateway-server-cert
          readOnly: true
        - mountPath: /etc/nginx/nginx.conf
          name: nginx-config
          readOnly: true
          subPath: nginx.conf
        livenessProbe:
          httpGet:
            path: /v1/healthz
            port: 9000
          initialDelaySeconds: 3
          periodSeconds: 3
      serviceAccountName: jobs-api
      volumes:
      - name: nsswitch
        hostPath:
          path: /etc/nsswitch.conf
          type: File
      - hostPath:
          path: /data/project
          type: Directory
        name: home
      - hostPath:
          path: /etc/wmcs-project
          type: File
        name: wmcs-project
      - hostPath:
          path: /var/lib/sss/pipes
          type: Directory
        name: sssd-pipes
      - configMap:
          items:
            - key: harbor.json
              path: harbor.json
          name: harbor-config
        name: harbor-config
      - configMap:
          items:
            - key: nginx.conf
              path: nginx.conf
          name: nginx-config
        name: nginx-config
      - name: api-gateway-server-cert
        secret:
          secretName: {{ .Release.Name }}-api-gateway-server
