# Copyright (C) 2022 Arturo Borrero Gonzalez <aborrero@wikimedia.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import functools
import json
import logging
import urllib.parse
from dataclasses import dataclass
from enum import Enum

import requests
import yaml
from toolforge_weld.kubernetes import K8sClient

from .error import TjfError

LOGGER = logging.getLogger(__name__)


class ImageType(Enum):
    STANDARD = "standard"
    BUILDPACK = "buildpack"

    def use_standard_nfs(self) -> bool:
        return self != ImageType.BUILDPACK


@dataclass(frozen=True)
class Image:
    type: ImageType
    canonical_name: str
    aliases: list[str]
    container: str
    state: str


@dataclass(frozen=True)
class HarborConfig:
    host: str
    protocol: str = "https"


# The ConfigMap is only read at startup. Restart the webservice to reload the available images
AVAILABLE_IMAGES: list[Image] = []


CONFIG_VARIANT_KEY = "jobs-framework"
# TODO: make configurable
CONFIG_CONTAINER_TAG = "latest"

HARBOR_CONFIG_PATH = "/etc/jobs-api/harbor.json"
HARBOR_IMAGE_STATE = "stable"


def get_harbor_project(tool: str) -> str:
    return f"tool-{tool}"


def update_available_images(client: K8sClient) -> None:
    configmap = client.get_object("configmaps", "image-config")
    yaml_data = yaml.safe_load(configmap["data"]["images-v1.yaml"])

    AVAILABLE_IMAGES.clear()

    for name, data in yaml_data.items():
        if CONFIG_VARIANT_KEY not in data["variants"]:
            continue

        container = data["variants"][CONFIG_VARIANT_KEY]["image"]
        image = Image(
            type=ImageType.STANDARD,
            canonical_name=name,
            aliases=data.get("aliases", []),
            container=f"{container}:{CONFIG_CONTAINER_TAG}",
            state=data["state"],
        )

        AVAILABLE_IMAGES.append(image)

    if len(AVAILABLE_IMAGES) < 1:
        raise TjfError("Empty list of available images")


@functools.lru_cache(maxsize=None)
def get_harbor_config() -> HarborConfig:
    with open(HARBOR_CONFIG_PATH, "r") as f:
        data = json.load(f)
    return HarborConfig(
        host=data["host"],
        protocol=data.get("protocol", HarborConfig.protocol),
    )


def get_harbor_images_for_name(project: str, name: str) -> list[Image]:
    config = get_harbor_config()

    encoded_project = urllib.parse.quote_plus(project)
    encoded_name = urllib.parse.quote_plus(name)

    try:
        response = requests.get(
            f"{config.protocol}://{config.host}/api/v2.0/projects/{encoded_project}/repositories/{encoded_name}/artifacts",
            params={
                # TODO: pagination if needed
                "page": "1",
                "page_size": "25",
            },
            headers={
                "User-Agent": f"jobs-framework-api python-requests/{requests.__version__}",
            },
            timeout=5,
        )
        response.raise_for_status()
    except requests.exceptions.HTTPError:
        LOGGER.warning("Failed to load Harbor tags for %s/%s", project, name, exc_info=True)
        return []

    images: list[Image] = []
    for artifact in response.json():
        if artifact["type"] != "IMAGE":
            continue
        if not artifact["tags"]:
            continue

        for tag in artifact["tags"]:
            tag_name = tag["name"]
            images.append(
                Image(
                    type=ImageType.BUILDPACK,
                    canonical_name=f"{project}/{name}:{tag_name}",
                    aliases=[],
                    container=f"{config.host}/{project}/{name}:{tag_name}",
                    state=HARBOR_IMAGE_STATE,
                )
            )

    return images


def get_harbor_images(tool: str) -> list[Image]:
    config = get_harbor_config()

    harbor_project = get_harbor_project(tool=tool)
    encoded_project = urllib.parse.quote_plus(harbor_project)

    try:
        response = requests.get(
            f"{config.protocol}://{config.host}/api/v2.0/projects/{encoded_project}/repositories",
            params={
                "with_tag": "true",
                # TODO: pagination if needed
                "page": "1",
                "page_size": "25",
            },
            headers={
                "User-Agent": f"jobs-framework-api python-requests/{requests.__version__}",
            },
            timeout=5,
        )
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        if (not e.response) or e.response.status_code != 401:
            # You seem to get a 401 when the project does not exist for whatever reason
            # don't log those, they are usually typos
            LOGGER.warning(
                "Failed to load Harbor images for project %s", harbor_project, exc_info=True
            )
        return []

    images: list[Image] = []

    for repository in response.json():
        name = repository["name"][len(harbor_project) + 1 :]
        images.extend(get_harbor_images_for_name(project=harbor_project, name=name))

    return images


def image_by_name(name: str) -> Image:
    for image in AVAILABLE_IMAGES:
        if image.canonical_name == name or name in image.aliases:
            return image

    if "/" in name and ":" in name:
        # harbor image?
        project, image_name = name.split("/", 1)
        image_name, _ = image_name.split(":", 1)
        for image in get_harbor_images_for_name(project, image_name):
            if image.canonical_name == name:
                return image

    raise ValueError(f"No such image '{name}'")


def image_by_container_url(url: str) -> Image:
    for image in AVAILABLE_IMAGES:
        if image.container == url:
            return image

    harbor_config = get_harbor_config()
    if url.startswith(harbor_config.host):
        # we assume images loaded from URLs exist

        image_name_with_tag = url[len(harbor_config.host) + 1 :]
        return Image(
            type=ImageType.BUILDPACK,
            canonical_name=image_name_with_tag,
            aliases=[],
            container=url,
            state=HARBOR_IMAGE_STATE,
        )

    raise TjfError("Unable to find image in the supported list or harbor", data={"image": url})
