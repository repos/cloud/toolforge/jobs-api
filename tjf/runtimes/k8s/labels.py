# Copyright (C) 2021 Arturo Borrero Gonzalez <aborrero@wikimedia.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from __future__ import annotations

from typing import Dict, Optional

from toolforge_weld.kubernetes import MountOption


def generate_labels(
    *,
    jobname: Optional[str],
    tool_name: str,
    type: Optional[str],
    filelog: bool = False,
    emails: str | None = None,
    version: bool = True,
    mount: MountOption | None = None,
) -> Dict[str, str]:
    obj = {
        "toolforge": "tool",
        "app.kubernetes.io/managed-by": "toolforge-jobs-framework",
        "app.kubernetes.io/created-by": tool_name,
    }

    if version:
        obj["app.kubernetes.io/version"] = "2"

    if type is not None:
        obj["app.kubernetes.io/component"] = type

    if jobname is not None:
        obj["app.kubernetes.io/name"] = jobname

    if filelog is True:
        obj["jobs.toolforge.org/filelog"] = "yes"

    if emails is not None:
        obj["jobs.toolforge.org/emails"] = emails

    if mount:
        obj.update(mount.labels)

    return obj


def labels_selector(
    user_name: str, job_name: str | None = None, type: str | None = None
) -> Dict[str, str]:
    return generate_labels(
        jobname=job_name,
        tool_name=user_name,
        type=type,
        filelog=False,
        emails=None,
        version=False,
    )
